<div class="container container-shrink">
    <div class="yellow-center">
        <div class="yellow-holder">
            <div class="curv"></div>
            <div class="yellow yellow-animaition">
            </div>
        </div>
    </div>
    <div class="orange-center">
        <div class="orange-holder">
            <div class="orange">
            </div>
        </div>
    </div>
    <div class="red-center">
        <div class="red-holder">
            <div class="red"> 
            </div>
        </div>
    </div>
</div>