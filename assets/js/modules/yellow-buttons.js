define(['jquery'], function($){
    var thePage;
    var yellowPagesIpad = function(){
        $('.container .yellow').removeClass('yellow-animaition');
        setTimeout(function(){
            $('.container').addClass('yellow-to-page-ipad');
        }, 20);
        $('.container-shrink').addClass('hide-yellow');
            $('.' + thePage + '-page .body-text-holder').removeClass('isHidden');
        setTimeout(function(){
            $('.' + thePage + '-page').addClass('show-this-page');
        }, 1400);
        $('.back-to-list').addClass('show-back-to-list');
        $('body').addClass('body-no-scroll');
        $('.logo-holder').addClass('shrink-the-logo');
        $('.title').addClass('shrink-the-title');
        $('.key-holder').addClass('shrink-the-key')
    };
    $('.yellow-btn .text-holder').on('click', function(){
        thePage = $(this).parent().parent().attr('name');
        yellowPagesIpad();
    });
});