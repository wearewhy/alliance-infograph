define(['jquery'], function($){ 
        var thePage = '';

        var redPagesIpad = function(){
            $('.container .red').removeClass('yellow-animaition');
            setTimeout(function(){
                $('.' + thePage + '-page .body-text-holder').removeClass('isHidden');
                $('.container').addClass('red-to-page-ipad');
            }, 20);
            setTimeout(function(){
                $('.' + thePage + '-page').addClass('show-this-page');
            }, 1400);
            $('#js-back').addClass('show-back-to-list');
            $('body').addClass('body-no-scroll');
            $('.logo-holder').addClass('shrink-the-logo');
            $('.title').addClass('shrink-the-title');
            $('.key-holder').addClass('shrink-the-key')
        };
        $('.red-btn .text-holder').on('click', function(){
            thePage = $(this).parent().parent().attr('name');
            redPagesIpad();
        });
});