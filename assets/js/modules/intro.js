define(['jquery', 'helpers'], function($, Helpers){

  var Intro = {
    slideUp : function(){
      setTimeout(function(){
        $('.yellow-holder').addClass('slideup');
      }, 10);
      setTimeout(function(){
        $('.orange-holder').addClass('slideup');
      }, 100);
      setTimeout(function(){
        $('.red-holder').addClass('slideup');
      }, 200);

      $('.red-holder').one(Helpers.whichTransition(), function(){
        Intro.flare();
        Intro.contentshow();
      });
    },
    contentshow : function(){
      var timer = setTimeout(function(){
        $('.yellow-holder').addClass('yellowflare');
        $('.yellow-center').addClass('show-content');
        $('.yellow-center').one(Helpers.whichTransition(), function(){
          $('.orange-center').addClass('show-content');
          $('.orange-center').one(Helpers.whichTransition(), function(){
            $('.red-center').addClass('show-content');
            $('.red-center').one(Helpers.whichTransition(), function(){
              $('.container').css('overflow','initial');
              $('.title-container,.key-holder,.logo-holder').addClass('show-content');
              $('#js-header').addClass('showdownload');
              clearTimeout(timer);
            });
          });
        });
      },40);
    },
    flare : function(){
      setTimeout(function(){
        $('.yellow').addClass('yellowflare');
      },10 );
      setTimeout(function(){
        $('.yellow').removeClass('yellowflare');

        $('.orange').addClass('orangeflare');
      }, 300);
      setTimeout(function(){
        $('.orange').removeClass('orangeflare');

        $('.red').addClass('redflare');
      }, 600);
      setTimeout(function(){
        $('.red').removeClass('redflare');
      },900);
    }
  }

  return Intro;
});