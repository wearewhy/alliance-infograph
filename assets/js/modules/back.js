define(['jquery', 'helpers'], function($, Helpers){
    $('#js-back').on('click',function(){   
        setTimeout(function(){
            $('.container').removeClass('red-to-page-ipad').removeClass('yellow-to-page-ipad');
            $('.body-text-holder').addClass('isHidden')
        }, 20);
        $('.show-this-page').removeClass('show-this-page');
        $('#js-back').removeClass('show-back-to-list'); 
        $('body').removeClass('body-no-scroll');
        $('.key-holder, .date, .title-container').fadeIn();
        $('.logo-holder').removeClass('shrink-the-logo');
        $('.title').removeClass('shrink-the-title');
        $('.key-holder').removeClass('shrink-the-key')
    });
});