define(['jquery', 'helpers'], function($, Helpers){
    var thePage, whichScreen;

    function displayContent(){

        if(whichScreen === true) {
            setTimeout(function(){
                $('.container').addClass('yellow-to-page-ipad');
            }, 20);
            $('.container-shrink').addClass('hide-yellow');
        }else {
            setTimeout(function(){
                $('.' + thePage + '-page .body-text-holder').removeClass('isHidden');
                $('.container').addClass('red-to-page-ipad');
            }, 20);
        }
        $('.' + thePage + '-page .body-text-holder').removeClass('isHidden');
        setTimeout(function(){
            $('.' + thePage + '-page').addClass('show-this-page');
        }, 1400);
        $('#js-back').addClass('show-back-to-list');
        $('body').addClass('body-no-scroll');
        $('.logo-holder').addClass('shrink-the-logo');
        $('.title').addClass('shrink-the-title');
        $('.key-holder').addClass('shrink-the-key')
    }

    $('.content-btn .text-holder').on('click', function(){
        thePage = $(this).parent().parent().attr('data-name');
        if($(document).width() > 660){
            $('html,body').animate({ scrollTop: 0 });
        }
        if($(this).parent('.content-btn').hasClass('yellow-btn')){
            whichScreen = true;
        }else {
            whichScreen = false;
        }
        displayContent();
    });
});