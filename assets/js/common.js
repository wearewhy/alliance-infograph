requirejs.config({
    baseUrl : 'assets/js',
    paths : {
        'jquery' : 'lib/jquery',
        'home' : 'home',
        'infograph' : 'modules/infograph',
        'yellowButtons' : 'modules/yellow-buttons',
        'redButtons' : 'modules/red-buttons',
        'intro' : 'modules/intro',
        'helpers' : 'modules/helpers',
        'back'  : 'modules/back',
        'display-content'  : 'modules/display-content'
    }
});
