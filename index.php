<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>UK Evidence Ecosystem</title>
        <script>
          (function(d) {
            var config = {
              kitId: 'zjn0vbu',
              scriptTimeout: 3000,
              async: true
            },
            h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
          })(document);
        </script>
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <meta name="viewport" content="width=device-width initial-scale=1, maximum-scale=1, user-scalable=no" />
    </head> 
    <body>
        <div id="js-header" class="header">
            <div class="header-contain">
                <div id="js-back" class="back-to-list">
                    <div class="left"></div><div class="midal"></div><div class="right"></div>
                    <p>back</p>
                </div>
                <div class="icon-holder">
                    <a href="http://alliance4usefulevidence.org/infographic/downloads/Alliance_info_graphic.pdf" target="_blank" download="Alliance_info_graphic.pdf">
                        <div class="icon"></div>
                        <p>Download as PDF</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="title-container">
                <h1 class="title">
                    UK Evidence Ecosystem for social policy
                </h1>
            </div>
            <div class="key-holder">
                <div class="producers key-holder-item">
                    <div class="shape-holder key-holder-item-inner">
                        <span class="key-holder-c circul"></span>
                    </div><div class="key-holder-item-inner">Producers</div>
                </div><div class="consumers key-holder-item">
                    <div class="shape-holder key-holder-item-inner">
                        <span class="key-holder-s squer"></span>
                    </div><div class="key-holder-item-inner">Consumers</div>
                </div><div class="intermediaries key-holder-item">
                    <div class="shape-holder key-holder-item-inner">
                        <span class="key-holder-t triangle"></span>
                    </div><div class="key-holder-item-inner">Intermediaries</div>
                </div>
            </div>
            <div class="logo-holder">
                <div class="logo"></div>
            </div>
            <div class="yellow-center">
                <h1>Non-Government Bodies</h1>
                <span class="title-underline">.........................</span>
                <div class="yellow-holder">
                    <div class="curv"></div>
                    <div class="yellow">
                        <div class="content-btn yellow-btn" data-name="research-summaries">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/st.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Research summaries for policymakers and practitioners</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. Campbell Collaboration, Best Evidence Encyclopaedia UK<br/>more...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn yellow-btn" data-name="independant-research">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/sq.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Independent research organisations</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. Behavioural Insights Team, Institute for Fiscal Studies, Social Research Unit<br/>more...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn yellow-btn" data-name="professional-membership">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/qst.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Professional membership bodies &amp; learned societies</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. Society for Evidence Based Policing, Evidence Based Teachers Network, Royal Statistical Society<br/>more...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="universities">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/s.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Universities</h2>
                                    <div class="text-holder-inner">
                                        <p>130+ UK Higher Education Institutions, c25,000 social science researchers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn yellow-btn" data-name="campaigns-research">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/t.png);"></div>
                                <div class="text-holder">
                                    <h2>Campaigns</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. Coalition for Evidence Based Education</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn yellow-btn" data-name="consultants-business">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/q.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Consultants &amp; business</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. Social Innovation Partnership, Triple P Ltd <br/>more...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn yellow-btn" data-name="charities-enterprise">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/qt.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Charities &amp; social enterprise</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. Pro Bono Economics, Atlantic Philanthropies, Nesta<br/>more...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><div class="orange-center">
                <h1>What Works Centres</h1>
                <span class="title-underline">.........................</span>
                <div class="orange-holder">
                    <div class="orange">
                        <div id="public-policy-wales">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/t.png);">
                                </div>
                                <a class="external-btn" href="http://ppiw.org.uk/" target="_blank">
                                    <div class="text-holder">
                                        <h2>Public Policy Institute for Wales</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="what-works-scotland">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/t.png);">
                                </div>
                                <a class="external-btn" href="http://whatworksscotland.ac.uk/" target="_blank">
                                    <div class="text-holder">
                                        <h2>What Works Scotland</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="ageing-better">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/st.png);">
                                </div>
                                <a class="external-btn" href="http://www.centreforageingbetter.com/" target="_blank">
                                    <div class="text-holder">
                                        <h2>Centre for Ageing Better</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="centre-for-wellbeing">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/t.png);">
                                </div>
                                <a class="external-btn" href="http://whatworkswellbeing.org/" target="_blank">
                                    <div class="text-holder">
                                        <h2>What Works Centre for Wellbeing</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="intervention-foundation">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/t.png);">
                                </div>
                                <a class="external-btn" href="http://www.eif.org.uk/" target="_blank">
                                    <div class="text-holder">
                                        <h2>Early Intervention Foundation</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="education-endowment-foundation">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/st.png);">
                                </div>
                                <a class="external-btn" href="https://educationendowmentfoundation.org.uk/" target="_blank">
                                    <div class="text-holder">
                                        <h2>Education Endowment Foundation</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="local-economic-growth">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/st.png);">
                                </div>
                                <a class="external-btn" href="http://www.whatworksgrowth.org/" target="_blank">
                                    <div class="text-holder">
                                        <h2>What Works Centre for Local Economic Growth</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="college-policing">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/t.png);"></div>
                                <a class="external-btn" href="http://whatworks.college.police.uk/Pages/default.aspx" target="_blank">
                                    <div class="text-holder">
                                        <h2>What Works Centre for Crime Reduction</h2>
                                        <div class="text-holder-inner">
                                            <p>College of Policing<br/>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="institute-for-health">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/qt.png);">
                                </div>
                                <a class="external-btn" href='https://www.nice.org.uk/' target="_blank">
                                    <div class="text-holder">
                                        <h2>National Institute for Health and Care Excellence</h2>
                                        <div class="text-holder-inner">
                                            <p>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="what-works-network">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/t.png);">

                                </div>
                                <a class="external-btn" href='https://www.gov.uk/guidance/what-works-network' target="_blank">
                                    <div class="text-holder">
                                        <h2>What Works Network</h2>
                                        <div class="text-holder-inner">
                                            <p>Cabinet Office<br/>Open site in new window</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><div class="red-center">
                <h1>Government Bodies</h1>
                <span class="title-underline">.........................</span>
                <div class="red-holder">
                    <div class="red red-animation">
                        <div class="content-btn red-btn" data-name="uk-government-departments">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/st.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>UK Government Departments</h2>
                                    <div class="text-holder-inner">
                                        <p>In-house researchers, statisticians, economists in Whitehall, Scotland, Wales and NI Governments e.g. Government Social Research Service<br /><a>more...</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn red-btn" data-name="arms-length-bodies">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/qst.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Arm's Length Bodies</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. Public Health England, Office for National Statistics,  National Audit Office<br/><a>more...</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn red-btn" data-name="devolved-parliaments">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/qt.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Westminster &amp; Devolved Parliaments &amp; Assemblies</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. P.O.S.T, House of Commons library, National Assembly for Wales Research Service<br/><a>more...</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn red-btn" data-name="local-gov">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/qst.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Local Government</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. In-house local authority researchers, LARIA, Improvement Service<br/><a>more...</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-btn red-btn" data-name="judiciary-legal-bodies">
                            <div class="infoholder">
                                <div class="shape-holder" style="background-image:url(assets/css/img/q.png);">
                                </div>
                                <div class="text-holder">
                                    <h2>Judiciary and legal bodies</h2>
                                    <div class="text-holder-inner">
                                        <p>e.g. The Supreme Court,
                                            Court of Appeal,
                                            Sentencing Council for England and Wales<br/><a>more...</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab uk-tab tap-red">
                <p>&pound;</p>
                <p>UK Gov</p>
            </div>
            <div class="tabs-mobile">
                <div class="tab welsh-tab bottom-tap">
                    <p>&pound;</p>
                    <p>Welsh Gov</p>
                </div>
                <div class="tab Scottish-tab bottom-tap">
                    <p>&pound;</p>
                    <p>Scottish Gov</p>
                </div>
                <div class="tab esrc-tab bottom-tap">
                    <p>&pound;</p>
                    <p>ESRC</p>
                </div>
                <div class="tab lottery-tab bottom-tap">
                    <p>&pound;</p>
                    <p>The Big Lottery Fund</p>
                </div>
            </div>
            <div class="page-holder">
                <div class="links-are-title">
                    <h3>Links To External Sites:</h3>
                </div>
                <div class="research-summaries-page">
                    <div class="body-text-holder isHidden">
                        <h1>Non-Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="research-summaries" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/st.png);">
                                    </div>
                                    <div class="text-holder">
                                        <h2>Research summaries for policymakers and practitioners</h2>
                                        <div class="page-text">
                                            <p>
                                                Below are research summaries aimed at policymakers and practitioners - NOT researchers. 
                                                Whether they are research hubs, portals, toolkits, clearinghouses, guidebooks or databases, 
                                                these repositories of research are relevant and accessible to non-researchers. Most of these 
                                                are freely available. However, those denoted with * are websites that do require a paid-for 
                                                subscription. Only those resources with a strong UK presence are included. We have not included 
                                                some of the world-class and influential research databases such as the <a href="https://www.ukdataservice.ac.uk/" target="_BLANK">UK Data Service</a> or <a href="https://www.ukdataservice.ac.uk/about-us/our-rd/closer" target="_BLANK">CLOSER</a>. 
                                                Although their data is useful for policymakers, commissioners and practitioners, their outputs 
                                                are not directly accessible to those outside research - unless through third-party intermediaries 
                                                or analysis by other specialists. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <h2>Cross-cutting social policy &amp; practice</h2>
                                <h3>The Idox Information Service*</h3>
                                <p>
                                    Holds over 200,000 summarised research resources, increasing by 1,000 abstracts 
                                    per month, across 30 local authority public policy areas including planning, 
                                    regeneration, social policy and economic development. <a href="http://informationservice.idoxgroup.com/iii/v2/about/index.html" target="_blank">( Visit site )</a>
                                </p>
                                <h3>Campbell Collaboration</h3>
                                <p>
                                    A comprehensive website for lists of systematic reviews, they cover 
                                    education, crime and justice, social welfare and international development. 
                                    Based in Oslo, they are an international network of volunteers.
                                    <a href="http://www.campbellcollaboration.org/" target="_blank">( Visit site )</a>
                                </p>
                                <h3>EPPI-Centre (The Evidence for Policy and Practice Information and Co-ordinating Centre)</h3>
                                <p>
                                    A 'knowledge library' of systematic reviews range across social policy and health. 
                                    The large number of systematic reviews conducted by and supported by 
                                    the EPPI-Centre have contributed to a broad knowledge base. <a href="http://eppi.ioe.ac.uk/cms/Default.aspx?tabid=67" target="_blank">( Visit site )</a>
                                </p>
                                <h3>Early Intervention Guidebook</h3>
                                <p>
                                    An interactive online resource for those who wish to find out more about 
                                    how to commission and deliver effective early intervention. It continually 
                                    expands as they gain more information about 'what works' for children and 
                                    commissioning children's services. A key feature of the Guidebook is the 
                                    Programmes Library that contains the details of 50 programmes that have been 
                                    successfully implemented in the UK. <a href="http://guidebook.eif.org.uk/" target="_blank">( Visit site )</a>
                                </p>
                                <h3>Understanding Society</h3>
                                <p>
                                    Also known as the UK Household Longitudinal Study (UKHLS), Understanding 
                                    Society tracks social and economic change in the UK. Started in 2009 
                                    with an initial sample of 40,000 households, it captures important 
                                    information about people's social and economic circumstances, attitudes, 
                                    behaviours and health. They have a Policy Unit that can help with 
                                    effective data-use by government departments, public bodies, charities 
                                    or think-tanks, and garner evidence and experts. <a href="https://www.understandingsociety.ac.uk/" target="_blank">( Visit site )</a>
                                </p>
                                <h3>NHS Evidence</h3>
                                <p>
                                    Index of evidence-based information from ‘trustworthy and accredited' sources. <a href="http://www.evidence.nhs.uk/" target="_blank">( Visit site )</a>
                                </p>
                                <h3>SCVO (Scottish Council for Voluntary Organisations)</h3>
                                <p>
                                    Evidence Library Registration is required to login, but it is free. 
                                    Funded by the Scottish Government, The Evidence Library is interactive 
                                    because it also facilitates comment and discussion, the exchanging 
                                    of ideas and the sharing of knowledge. <a href="http://scvoevidencelibrary.org.uk/Home/Home.aspx" target="_blank">( Visit site )</a>
                                </p>
                                <h3>Social Policy and Practice*</h3>
                                <p>
                                    A one-stop-shop for research, analysis and discussion of health and social 
                                    policy, the database holds over 350,000 abstracts on social policy, 30% 
                                    of content is grey literature. The database is made up from selected content 
                                    from the major providers: Idox Information Service; Social Care Institute 
                                    for Excellence; National Children's Bureau; and the Centre for Policy on Ageing, 
                                    (NSPCC data is currently also being added) this is run through Ovid Technologies 
                                    on a commercial basis. <a href="http://www.spandp.net/" target="_blank">( Visit site )</a>
                                </p>
                                <h2>Social care</h2>
                                <h3>Social Care online</h3>
                                <p>
                                    An electronic library delivered by the Social Care Institute for Excellence, 
                                    with over 150,000 abstracts covering all aspects of social care, social 
                                    welfare and social policy, currently free to access through a grant from 
                                    the Department of Health. It covers information of people with social care needs; 
                                    those receiving care services; key issues such as integrated services, 
                                    safeguarding or legislation; Government policy and social work and the 
                                    social care workforce. <a href="http://www.scie-socialcareonline.org.uk/" target="_blank">( Visit site )</a>
                                </p>
                            </div><div class="text">
                                <h2>Education, children and young people</h2>
                                <h3>Investing in Children</h3>
                                <p>
                                    A one-stop-shop for information for over 100 interventions relating to 
                                    children's services, bringing together information about what works 
                                    and cost-benefit analysis. <a href="http://investinginchildren.eu/interventions" target="_blank">( Visit site )</a>
                                </p>
                                <h3>Best Evidence Encyclopaedia UK</h3>
                                <p>
                                    Focused on education, the website gives information about the strength 
                                    of evidence supporting a variety of programmes available for pupils of 
                                    both primary and secondary school-age. The site provides summaries of 
                                    scientific reviews produced by various authors and organisations, as well 
                                    as links to the full texts of each review. <a href="http://www.bestevidence.org.uk/index.html" target="_blank">( Visit site )</a>
                                </p>
                                <h3>ChildData*</h3>
                                <p>
                                    From the National Children's Bureau charity, ChildData is the only bibliographic 
                                    database covering all aspects of research and practice in young people's social 
                                    care. It is now only available through Social Policy and Practice. Content 
                                    includes reports, research and resources on early childhood; education and learning; 
                                    health and wellbeing; involving young people; play; sector improvement; SEN 
                                    and disability; and vulnerable children. <a href="http://www.ovid.com/site/catalog/databases/1859.jsp" target="_blank">( Visit site )</a>
                                </p>
                                <h3>NSPCC Research and Resources</h3>
                                <p>
                                    A free resource for those working in the childcare and protection sector, it includes 
                                    all forms of case reviews and literature and current awareness bulletins. They 
                                    provide research and statistics on child abuse; working with children and families; 
                                    policy and guidance on child protection; resources; case reviews and learning materials.  <a href="http://www.nspcc.org.uk/services-and-resources/research-and-resources/" target="_blank">( Visit site )</a>
                                </p>
                                <h3>Teaching and Learning Toolkit</h3>
                                <p>
                                    A summary of research relevant to teaching kids aimed 5 to 16 years. The Sutton 
                                    Trust and Education Endowment Foundation Teaching have created this Toolkit 
                                    to provide guidance for teachers and schools on how to use their resources 
                                    to improve the attainment of disadvantaged pupils. The Toolkit currently covers 
                                    34 topics, each summarised in terms of their average impact on attainment, the 
                                    strength of the evidence supporting them and their cost. For those working in 
                                    Early Years, there is also another dedicated toolkit.  <a href="https://educationendowmentfoundation.org.uk/toolkit/toolkit-a-z" target="_blank">( Visit site )</a>
                                </p>
                                <h3>Evidence 4 Impact</h3>
                                <p>
                                    The UK's Evidence 4 Impact (E4I) provides a simple evidence rating system for 
                                    programmes relating to schools, along with concise evidence summaries, which 
                                    enable you to make a well-informed judgement on the extent to which a programme's 
                                    effectiveness is 'proven'. Central to E4I is a comprehensive database of programmes 
                                    available in the UK, including details on their effectiveness and cost, together with 
                                    links to the providers and experts who can offer further support. The database can 
                                    be easily searched by key stage, subject area and targeted group, so that results 
                                    are tailored to the specific needs of your class or school. <a href="http://www.evidence4impact.org.uk/about.php" target="_blank">( Visit site )</a>
                                </p>
                                <h2>Ageing</h2>
                                <h3>Ageinfo</h3>
                                <p>    
                                    The only UK database covering all aspects of ageing and older age, including research and
                                    practice in the social and health issues of older age. A bibliographic database of 
                                    over 55,000 books, articles and reports from the specialist collection held by 
                                    the Centre for Policy on Ageing. Please note that the database does not give access 
                                    directly the publication - it only finds titles. <a href="http://ageinfo.cpa.org.uk/scripts/open-ai/hfclient.exe?A=open-ai&sk" target="_blank">( Visit site )</a>
                                </p>
                                <h2>Crime</h2>
                                <h3>Crime Reduction toolkit</h3>
                                <p>
                                    Using over 300 systematic reviews, and covering 60 different crime reduction 
                                    interventions, the toolkit provides easy access to the crime reduction evidence 
                                    base. Users can weigh up evidence on the impact, cost and implementation of 
                                    different interventions and use this to help shape their crime reduction efforts.
                                    Note also the 2011 Scottish Government summaries of evidence on what works 
                                    in stopping people from reoffending, followed a few years later by the 2013 
                                    UK Government's summary of evidence on the same topic. <a href="http://whatworks.college.police.uk/toolkit/Pages/Toolkit.aspx" target="_blank">( Visit site )</a>
                                </p>
                                <p>
                                    <span style="font-family: 'proxima-nova',sans-serif!important; font-size: 14px!important"> Note also the 2011 
                                    <a target="_BLANK" href="http://www.gov.scot/Resource/0038/00385880.pdf" style="text-decoration: underline;">Scottish Government summaries of evidence</a> on what works 
                                    in stopping people from reoffending, followed a few years later by the 2013 
                                    <a target="_BLANK" href="https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/243718/evidence-reduce-reoffending.pdf" style="text-decoration: underline;">UK Government's summary of evidence</a> 
                                    on the same topic.</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="independant-research-page">
                    <div class="body-text-holder isHidden">
                        <h1>Non-Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="independant-research" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/psq.png);">
                                    </div>
                                    <div class="text-holder">
                                        <h2>Independent research organisations</h2>
                                       <div class="page-text">
                                            <p>
                                                Larger organisations with a primary interest in producing research and insights relating 
                                                to 'what works' in social policy. They have staff who are members of the Alliance for 
                                                Useful Evidence. The organisations have significant staff presence in the UK:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="http://www.behaviouralinsights.co.uk/" target="_blank">Behavioural Insights Team</a></li>
                                    <li><a href="http://cesi.org.uk/" target="_blank">Centre for Economic and Social Inclusion</a></li>
                                    <li><a href="http://www.centreforcities.org/" target="_blank">Centre for Cities (partner in Local Economic Growth What Works Centre)</a></li>
                                    <li><a href="http://comres.co.uk/" target="_blank">ComRes</a></li>
                                    <li><a href="http://www.curee.co.uk/" target="_blank">CUREE (Centre for Research and Use of Evidence in Education)</a></li>
                                    <li><a href="http://www.evaluationsupportscotland.org.uk/" target="_blank">Evaluation Support Scotland</a></li>
                                    <li><a href="http://www.employment-studies.co.uk/" target="_blank">Institute for Employment Studies</a></li>
                                    <li><a href="http://www.ifs.org.uk/" target="_blank">Institute for Fiscal Studies</a></li>
                                    <li><a href="http://www.instituteforgovernment.org.uk/" target="_blank">Institute for Government</a></li>
                                    <li><a href="https://www.iser.essex.ac.uk/ " target="_blank">Institute of Social and Economic Research </a></li>
                                    <li><a href="https://www.ipsos-mori.com/" target="_blank">Ipsos MORI</a></li>
                                    <li><a href="http://www.iriss.org.uk/" target="_blank">IRISS (Institute for Research and Innovation in Social Services)</a></li>
                                    <li><a href="http://www.povertyactionlab.org/europe" target="_blank">JPAL-Europe</a></li>
                                    <li><a href="http://www.natcen.ac.uk/" target="_blank">NatCen Social Research</a></li>
                                    <li><a href="https://www.nfer.ac.uk/" target="_blank">National Foundation for Educational Research</a></li>
                                    <li><a href="http://www.niesr.ac.uk/" target="_blank">National Institute of Economic and Social Research</a></li>
                                    <li><a href="http://www.psi.org.uk/" target="_blank">Policy Studies Institute</a></li>
                                    <li><a href="http://ppiw.org.uk/" target="_blank">Public Policy Institute for Wales</a></li>
                                    <li><a href="http://www.rand.org/randeurope.html" target="_blank">RAND Europe</a></li>
                                    <li><a href="http://dartington.org.uk/about/" target="_blank">Social Research Unit at Dartington</a></li>
                                    <li><a href="http://www.kingsfund.org.uk/" target="_blank">The King's Fund</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="consultants-business-page">
                    <div class="body-text-holder isHidden">
                        <h1>Non-Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="consultants-business" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pq.png);"></div>
                                    <div class="text-holder">
                                        <h2>Consultants &amp; business</h2>
                                       <div class="page-text">
                                            <p>This list includes larger consultancies, support services, 
                                            not-for-profit and commercial organisations providing commercial 
                                            services and programmes on 'what works' in social policy with staff 
                                            who are members of the Alliance for Useful Evidence. It does not include 
                                            the many influential sole trader consultants or smaller organisations. 
                                            Note that many charities also provide consultancy services and may also 
                                            be included in the section on Charities &amp; Social Enterprises</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="http://www.arup.com/" target="_blank">Arup</a></li>
                                    <li><a href="http://www.capita.co.uk/" target="_blank">Capita PLC</a></li>
                                    <li><a href="http://www2.deloitte.com/uk/en.html" target="_blank">Deloitte</a></li>
                                    <li><a href="http://www.uk.ecorys.com/" target="_blank">Ecorys UK</a></li>
                                    <li><a href="http://www.frontier-economics.com/selectasite/" target="_blank">Frontier Economics</a></li>
                                    <li><a href="http://www.icfi.com/regions/europe/united-kingdom" target="_blank">ICF GHK</a></li>
                                    <li><a href="http://www.idoxgroup.com/" target="_blank">IDOX</a></li>
                                    <li><a href="http://www.kpmg.com/uk/en/pages/default.aspx" target="_blank">KPMG</a></li>
                                    <li><a href="http://www.thinknpc.org/" target="_blank">New Philanthropy Capital (NPC)</a></li>
                                    <li><a href="http://www.pwc.co.uk/" target="_blank">PwC</a></li>
                                    <li><a href="http://stratagem-ni.com/" target="_blank">Stratagem Northern Ireland</a></li>
                                    <li><a href="http://www.technopolis-group.com/" target="_blank">Technopolis</a></li>
                                    <li><a href="http://tsip.co.uk/" target="_blank">The Social Innovation Partnership</a></li>
                                    <li><a href="http://www.triplep-parenting.uk.net/uk-en/home/?cdsid=unthlpoottmioqpofqhrgpjrrkglgsko" target="_blank">Triple P Ltd</a></li>
                                    <li><a href="http://www.which.co.uk/" target="_blank">Which?</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="professional-membership-page">
                    <div class="body-text-holder isHidden">
                        <h1>Non-Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="professional-membership" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pqst.png);">
                                    </div>
                                    <div class="text-holder">
                                       <h2>Professional membership bodies &amp; learned societies</h2>
                                       <div class="page-text">
                                            <p>These are professional membership bodies and learned societies 
                                                with an interest in applying research in social policy and practice. 
                                                Individual staff at these organisations are members of the Alliance for Useful Evidence:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="https://acss.org.uk/" target="_blank">Academy of Social Sciences</a></li>
                                    <li><a href="http://www.auril.org.uk/" target="_blank">AURIL -representing practitioners in knowledge exchange</a></li>
                                    <li><a href="http://www.britac.ac.uk/" target="_blank">British Academy</a></li>
                                    <li><a href="http://www.bps.org.uk/" target="_blank">British Psychological Society</a></li>
                                    <li><a href="http://www.managers.org.uk/" target="_blank">Chartered Management Institute</a></li>
                                    <li><a href="http://www.ebtn.org.uk/" target="_blank">Evidence Based Teachers Network</a></li>
                                    <li><a href="http://www.housing.org.uk/" target="_blank">National Housing Federation</a></li>
                                    <li><a href="http://www.nmc.org.uk/" target="_blank">Nursing and Midwifery Council</a></li>
                                    <li><a href="http://probation-institute.org/" target="_blank">Probation Institute</a></li>
                                    <li><a href="http://www.parnglobal.com/" target="_blank">Professional Associations Research Network</a></li>
                                    <li><a href="http://www.rss.org.uk/" target="_blank">Royal Statistical Society</a></li>
                                    <li><a href="http://www.rtpi.org.uk/" target="_blank">Royal Town Planning Institute</a></li>
                                    <li><a href="http://www.rcn.org.uk/" target="_blank">Royal College of Nursing</a></li>
                                    <li><a href="http://www.sciencecouncil.org/" target="_blank">Science Council</a></li>
                                    <li><a href="http://socialvalueuk.org/home/social-value-international-uk" target="_blank">Social Value UK</a></li>
                                    <li><a href="http://www.sebp.police.uk/" target="_blank">Society for Evidence Based Policing</a></li>
                                    <li><a href="http://www.scie.org.uk/" target="_blank">Social Care Institute for Excellence</a></li>
                                    <li><a href="http://the-sra.org.uk/" target="_blank">Social Research Association</a></li>
                                    <li><a href="http://tdtrust.org/" target="_blank">Teacher Development Trust</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="campaigns-research-page">
                    <div class="body-text-holder isHidden">
                        <h1>Non-Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="campaigns-research" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/t.png);">
                                    </div>
                                    <div class="text-holder">
                                       <h2>Campaigns</h2>
                                       <div class="page-text">
                                            <p>Advocacy, campaigns and initiatives to encourage social policy &amp; 
                                            practice to be informed by the smarter use of social science, science, and statistics.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="http://www.alliance4usefulevidence.org/" target="_blank">Alliance for Useful Evidence</a></li>
                                    <li><a href="http://sciencecampaign.org.uk/" target="_blank">Campaign for Science and Engineering</a></li>
                                    <li><a href="http://campaignforsocialscience.org.uk/" target="_blank">Campaign for Social Science</a></li>
                                    <li><a href="https://theconversation.com/uk" target="_blank">The Conversation UK</a></li>
                                    <li><a href="http://www.cebenetwork.org/" target="_blank">Coalition for Evidence Based Education</a></li>
                                    <li><a href="http://educationmediacentre.org/" target="_blank">Education Media Centre</a></li>
                                    <li><a href="https://fullfact.org/" target="_blank">Full Fact</a></li>
                                    <li><a href="http://www.historyandpolicy.org/" target="_blank">History and Policy</a></li>
                                    <li><a href="http://www.radstats.org.uk/" target="_blank">Radical Statistics</a></li>
                                    <li><a href="http://www.rss.org.uk/Default.aspx" target="_blank">Royal Statistical Society Parliament Counts campaign, data manifesto &amp; statistical literacy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="charities-enterprise-page">
                    <div class="body-text-holder isHidden">
                        <h1>Non-Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="charities-enterprise" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pqt.png);">
                                    </div>
                                    <div class="text-holder">
                                        <h2>Charities &amp; social enterprises</h2>
                                        <div class="page-text">
                                            <p>These are non-profit organisations committed to advancing the use of 
                                            research evidence for social policy and practice. Please note that some 
                                            charities below have commercial arms and are also companies limited by 
                                            guarantee. Staff are members of the Alliance for Useful Evidence.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="http://www.4children.org.uk/" target="_blank">4 Children</a></li>
                                    <li><a href="http://alcoholresearchuk.org/" target="_blank">Alcohol Research UK</a></li>
                                    <li><a href="http://www.alzheimers.org.uk/" target="_blank">Alzheimer's Society</a></li>
                                    <li><a href="http://www.atlanticphilanthropies.org/" target="_blank">Atlantic Philanthropies</a></li>
                                    <li><a href="http://www.barrowcadbury.org.uk/" target="_blank">Barrow Cadbury Trust</a></li>
                                    <li><a href="http://www.barnardos.org.uk/" target="_blank">Barnardo's</a></li>
                                    <li><a href="http://www.bigsocietycapital.com/" target="_blank">Big Society Capital</a></li>
                                    <li><a href="http://www.redcross.org.uk/" target="_blank">British Red Cross</a></li>
                                    <li><a href="http://www.carnegieuktrust.org.uk/home" target="_blank">Carnegie UK Trust</a></li>
                                    <li><a href="http://www.catch-22.org.uk/" target="_blank">Catch 22</a></li>
                                    <li><a href="http://www.chanceuk.com/" target="_blank">Chance UK</a></li>
                                    <li><a href="http://collaboratei.com/" target="_blank">Collaborate</a></li>
                                    <li><a href="http://www.drugscience.org.uk/" target="_blank">DrugScience</a></li>
                                    <li><a href="http://www.fim-trust.org/" target="_blank">Forces in Mind Trust</a></li>
                                    <li><a href="http://www.hact.org.uk/" target="_blank">HACT - the housing action charity</a></li>
                                    <li><a href="http://www.lifechangestrust.org.uk/" target="_blank">Life Changes Trust</a></li>
                                    <li><a href="http://www.macmillan.org.uk/" target="_blank">Macmillan Cancer Support</a></li>
                                    <li><a href="http://www.ncb.org.uk/" target="_blank">National Children's Bureau</a></li>
                                    <li><a href="http://www.nesta.org.uk/" target="_blank">Nesta</a></li>
                                    <li><a href="http://www.nominettrust.org.uk/" target="_blank">Nominet Trust</a></li>
                                    <li><a href="http://www.nspcc.org.uk/" target="_blank">NSPCC</a></li>
                                    <li><a href="http://www.thinknpc.org/" target="_blank">NPC (New Philanthropy Capital)</a></li>
                                    <li><a href="http://www.phf.org.uk/" target="_blank">Paul Hamlyn Foundation</a></li>
                                    <li><a href="http://www.probonoeconomics.com/" target="_blank">Pro Bono Economics</a></li>
                                    <li><a href="https://www.dartington.org/our-work/our-projects-initiatives/rip-and-ripfa/" target="_blank">Research in Practice for Adults</a></li>
                                    <li><a href="https://www.rip.org.uk/" target="_blank">Research in Practice</a></li>
                                    <li><a href="http://www.rethink.org/" target="_blank">Rethink Mental Illness</a></li>
                                    <li><a href="http://www.therobertsontrust.org.uk/" target="_blank">Robertson Trust</a></li>
                                    <li><a href="http://www.suttontrust.com/" target="_blank">Sutton Trust</a></li>
                                    <li><a href="http://www.childrenssociety.org.uk/" target="_blank">The Children's Society</a></li>
                                    <li><a href="http://tsip.co.uk/" target="_blank">The Social Innovation Partnership</a></li>
                                </ul>
                            </div><div class="text">
                                <h2>Charitable research funders</h2>
                                <p>Major funders of original social science that are outside government</p>
                                <ul>
                                    <li><a href="http://www.britac.ac.uk/" target="_blank">British Academy</a></li>
                                    <li><a href="http://www.nesta.org.uk/" target="_blank">Nesta</a></li>
                                    <li><a href="http://www.nuffieldfoundation.org/" target="_blank">Nuffield Foundation</a></li>
                                    <li><a href="https://www.jrf.org.uk/" target="_blank">Joseph Rowntree Foundation</a></li>
                                    <li><a href="http://www.wellcome.ac.uk/" target="_blank">Wellcome Trust</a></li>
                                    <li><a href="http://www.suttontrust.com/" target="_blank">Sutton Trust</a></li>
                                    <li><a href="https://www.leverhulme.ac.uk/" target="_blank">Leverhulme Trust</a></li>
                                </ul>
                                <h2>National charity umbrella bodies:</h2>
                                <ul>
                                    <li><a href="https://www.acevo.org.uk/" target="_blank">ACEVO The charity leaders network</a></li>
                                    <li><a href="http://www.acf.org.uk/" target="_blank">Association of Charitable Foundations</a></li>
                                    <li><a href="http://www.childreninscotland.org.uk/" target="_blank">Children in Scotland</a></li>
                                    <li><a href="http://www.alliance-scotland.org.uk/" target="_blank">Health and Social Care Alliance</a></li>
                                    <li><a href="http://www.ncvys.org.uk/" target="_blank">National Council for Voluntary Youth Services</a></li>
                                    <li><a href="https://www.ncvo.org.uk/" target="_blank">NCVO (National Council for Voluntary Organisations)</a></li>
                                    <li><a href="http://www.nicva.org/" target="_blank">NICVA (NI Council for Voluntary Action)</a></li>
                                    <li><a href="http://www.smallcharities.org.uk/" target="_blank">Small Charities Coalition</a></li>
                                    <li><a href="http://www.scvo.org.uk/" target="_blank">SCVO (Scottish Council for Voluntary Organisations)</a></li>
                                    <li><a href="http://www.volunteerscotland.net/" target="_blank">Volunteer Scotland</a></li>
                                    <li><a href="http://www.vhscotland.org.uk/" target="_blank">Voluntary Health Scotland</a></li>
                                    <li><a href="http://www.wcva.org.uk/" target="_blank">Wales Council for Voluntary Action</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="uk-government-departments-page">
                    <div class="body-text-holder isHidden">
                        <h1>Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="uk-government-departments" class="info">
                                <div class="infoholder three-lines">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pst.png);"></div>
                                    <div class="text-holder">
                                        <h2>UK Government departments</h2>
                                        <div class="page-text">
                                            <p>In-house researchers, statisticians, economists in Whitehall, Scotland, Wales and NI governments e.g. Government Social Research Service</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="http://gov.wales/about/civilservice/structure/management-groups/our-board/membership1/jamesprice/?lang=en" target="_blank">Chief Economist, Welsh Government</a></li>
                                    <li><a href="http://www.gov.scot/About/People/strategic-board/chief-economic-adviser" target="_blank">Chief Economic Adviser, Scottish Government</a></li>
                                    <li><a href="http://www.cso.scot.nhs.uk/" target="_blank">Chief Scientist Office, Scotland</a></li>
                                    <li><a href="http://gov.wales/statistics-and-research/?type=Social+Research&lang=en" target="_blank">Chief Social Research Officer, Welsh Government</a></li>
                                    <li><a href="http://gov.wales/statistics-and-research/about/user-engagement/chief-statisticians-update/?lang=en" target="_blank">Chief Statistician Wales</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/civil-service-government-economic-service" target="_blank">Government Economic Service</a></li>
                                    <li><a href="http://www.operational-research.gov.uk/recruitment" target="_blank">Government Operational Research Service</a></li>
                                    <li><a href="https://gss.civilservice.gov.uk/" target="_blank">Government Statistical Service</a></li>
                                    <li><a href="https://gss.civilservice.gov.uk/national-statistician-2/" target="https://gss.civilservice.gov.uk/national-statistician-2/">National Statistician</a></li>
                                    <li><a href="http://www.gov.scot/Topics/Research/About/Social-Research/about" target="_blank">Office of the Chief Researcher, Scotland</a></li>
                                </ul>
                                 <h3>Staff from these departments are members of the Alliance for Useful Evidence:</h3>
                                <ol>   
                                    <li>Cabinet Office</li>
                                    <li>Department for Business, Innovation &amp; Skills</li>
                                    <li>Department for Communities and Local Government</li>
                                    <li>Department for Culture, Media &amp; Sport</li>
                                    <li>Department for Education</li>
                                    <li>Department for Environment, Food &amp; Rural Affairs</li>
                                    <li>Department for International Development</li>
                                    <li>Department for Transport</li>
                                    <li>Department for Work and Pensions</li>
                                    <li>Department of Energy &amp; Climate Change</li>
                                    <li>Department of Health</li>
                                    <li>Foreign &amp; Commonwealth Office</li>
                                    <li>HM Treasury</li>
                                    <li>Home Office</li>
                                    <li>Ministry of Defence</li>
                                    <li>Ministry of Justice</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='arms-length-bodies-page info'>
                    <div class="body-text-holder isHidden">
                        <h1>Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="arms-length-bodies" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pqt.png);"></div>
                                    <div class="text-holder">
                                        <h2>Arm's length bodies</h2>
                                    </div>
                                    <div class="page-text">
                                        <p>A full list of UK arm's length bodies and agencies is 
                                        <a href="https://www.gov.uk/government/organisations" target="_BLANK">available here.</a> Individual staff who have joined as members of the Alliance for Useful Evidence:</p>
                                    </div>

                                </div>
                            </div>
                            <div class="text">
                               <ul>
                                    <li><a href="https://www.gov.uk/government/organisations/advisory-council-on-the-misuse-of-drugs" target="_blank">Advisory Council on the Misuse of Drugs</a></li>
                                    <li><a href="https://www.biglotteryfund.org.uk/" target="_blank">Big Lottery Fund</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/defence-science-and-technology-laboratory" target="_blank">Dstl (Defence Science and Technology Laboratory)</a></li>
                                    <li><a href="http://www.esrc.ac.uk/" target="_blank">Economic and Social Research Council</a></li>
                                    <li><a href="http://www.hse.gov.uk/" target="_blank">Health and Safety Executive</a></li>
                                    <li><a href="http://www.hscic.gov.uk/" target="_blank">Health &amp; Social Care Information Centre</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/intellectual-property-office" target="_blank">Intellectual Property Office</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/migration-advisory-committee" target="_blank">Migration Advisory Committee</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/natural-england" target="_blank">Natural England</a></li>
                                    <li><a href="http://www.nao.org.uk/" target="_blank">National Audit Office</a></li>
                                    <li><a href="http://budgetresponsibility.org.uk/" target="_blank">Office for Budget Responsibility</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/office-for-national-statistics" target="_blank">Office for National Statistics</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/national-offender-management-service" target="_blank">National Offender Management Service</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/regulatory-policy-committee" target="_blank">Regulatory Policy Committee</a></li>
                                    <li><a href="https://www.sentencingcouncil.org.uk/" target="_blank">Sentencing Council for England and Wales</a></li>
                                    <li><a href="http://www.sccyp.org.uk/" target="_blank">Scotland's Commissioner for Children and Young People</a></li>
                                    <li><a href="http://www.statisticsauthority.gov.uk/" target="_blank">UK Statistics Authority</a></li>
                                    <li><a href="http://www.audit.wales/" target="_blank">Wales Audit Office</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='devolved-parliaments-page info'>
                    <div class="body-text-holder isHidden">
                        <h1>Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="devolved-parliaments" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pqst.png);"></div>
                                    <div class="text-holder">
                                        <h2>Westminster &amp; Devolved Parliaments &amp; Assemblies</h2>
                                    </div>
                                    <div class="page-text">
                                        <p>e.g. P.O.S.T, House of Commons library, National Assembly for Wales Research Service</p>
                                    </div>

                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="http://www.parliament.uk/mps-lords-and-offices/offices/commons/commonslibrary/" target="_blank">House of Commons Library and its research service</a></li>
                                    <li><a href="http://www.parliament.uk/business/committees/committees-a-z/commons-select/" target="_blank">House of Commons select committees - complete list</a></li>
                                    <li><a href="http://www.parliament.uk/business/committees/committees-a-z/lords-select/" target="_blank">House of Lords select committees - complete list</a></li>
                                    <li><a href="http://www.ombudsman.org.uk/" target="_blank">Parliamentary and Health Service Ombudsman</a></li>
                                    <li><a href="http://www.assembly.wales/en/bus-home/research/Pages/research.aspx" target="_blank">National Assembly for Wales Research Service</a></li>
                                    <li><a href="http://www.parliament.uk/post" target="_blank">POST Parliamentary Office of Science and Technology</a></li>
                                    <li><a href="http://www.niassembly.gov.uk/assembly-business/research-and-information-service-raise/" target="_blank">RaISe - NI Assembly Research and Information Service</a></li>
                                    <li><a href="http://www.scottish.parliament.uk/parliamentarybusiness/research.aspx" target="_blank">Scottish Parliament Information Centre - briefings</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='judiciary-legal-bodies-page info'>
                    <div class="body-text-holder isHidden">
                        <h1>Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="judiciary-legal-bodies" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pq.png);"></div>
                                    <div class="text-holder">
                                        <h2>Judiciary and legal bodies</h2>
                                    </div>
                                    <div class="page-text">
                                        <p>With thanks to the Professor Rosemary Hunter FAcSS, Chair, Socio-Legal Studies Association for this list of users of social science:</p>
                                    </div>

                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="https://www.judiciary.gov.uk/related-offices-and-bodies/representative-bodies-2/association-hm-district-judges/" target="_blank">Association of Her Majesty's District Judges</a></li>
                                    <li><a href="https://www.cafcass.gov.uk/" target="_blank">Cafcass (Represents children in family court cases)</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/civil-procedure-rules-committee" target="_blank">Civil Procedure Rule Committee</a></li>
                                    <li><a href="https://www.judiciary.gov.uk/related-offices-and-bodies/advisory-bodies/cjc/" target="_blank">Civil Justice Council</a></li>
                                    <li><a href="https://www.justice.gov.uk/courts/rcj-rolls-building/court-of-appeal" target="_blank">Court of Appeal</a></li>
                                    <li><a href="https://www.judiciary.gov.uk/related-offices-and-bodies/representative-bodies-2/council-of-her-majestys-circuit-judges/" target="_blank">Council of Her Majesty's Circuit Judges</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/criminal-procedure-rule-committee" target="_blank">Criminal Procedure Rule Committee</a></li>
                                    <li><a href="https://www.judiciary.gov.uk/related-offices-and-bodies/advisory-bodies/fjc/" target="_blank">Family Justice Council</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/family-procedure-rule-committee" target="_blank">Family Procedure Rule Committee</a></li>
                                    <li><a href="https://www.judiciary.gov.uk/about-the-judiciary/training-support/judicial-college/" target="_blank">Judicial College</a></li>
                                    <li><a href="https://jac.judiciary.gov.uk/" target="_blank">Judicial Appointments Commission</a></li>
                                    <li><a href="https://www.gov.uk/government/groups/family-justice-board" target="_blank">Local Family Justice Boards</a></li>
                                    <li><a href="https://magistrates-association.org.uk/" target="_blank">Magistrates Association</a></li>
                                    <li><a href="https://www.sentencingcouncil.org.uk/" target="_blank">Sentencing Council for England &amp; Wales</a></li>
                                    <li><a href="https://www.supremecourt.uk/" target="_blank">The Supreme Court</a></li>
                                    <li><a href="http://www.ukawj.org/" target="_blank">UK Association of Women Judges</a></li>
                                    <li><a href="https://www.gov.uk/government/organisations/youth-justice-board-for-england-and-wales" target="_blank">Youth Justice Board for England and Wales</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>              

                <div class='local-gov-page info'>
                    <div class="body-text-holder isHidden">
                        <h1>Government Bodies</h1>
                        <span class="title-underline">.........................................................................</span>
                        <div class="spacing">
                            <div id="local-gov" class="info">
                                <div class="infoholder">
                                    <div class="shape-holder shape-holder-infoholder" style="background-image:url(assets/css/img/pqst.png);"></div>
                                    <div class="text-holder">
                                        <h2>Local Government</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <ul>
                                    <li><a href="http://www.improvementservice.org.uk/" target="_blank">Improvement Service</a></li>
                                    <li><a href="http://www.birmingham.ac.uk/schools/government-society/departments/local-government-studies/index.aspx" target="_blank">Institute of Local Government Studies</a></li>
                                    <li><a href="http://laria.org.uk/" target="_blank">LARIA (Local Area Research + Intelligence Association)</a></li>
                                    <li><a href="http://www.solace.org.uk/policy/network_evidence_based_policy/" target="_blank">Solace (The Society for Local Authority Chief Executives and Senior Managers) Evidence-Based Policy Network</a></li>
                                    <li><a href="http://www.whatworksgrowth.org/" target="_blank">What Works Centre for Local Economic Growth</a></li>
                                    <li><a href="http://whatworksscotland.ac.uk/" target="_blank">What Works Centre Scotland (focus on local areas in Scotland)</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
                    <br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
                <p>
                    In partnership with TSIP
                    <a class="tsip" href="http://tsip.co.uk/" target="_BLANK"></a>
                </p>
            </div>
        </div>

        <script src="assets/js/lib/require.js"></script>
        <script type="text/javascript">
            requirejs(["assets/js/common.js"], function(common) {
                    require(['home']);
            });
        </script>

    </body>
</html>